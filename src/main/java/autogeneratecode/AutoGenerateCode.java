package autogeneratecode;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.intellij.openapi.actionSystem.CommonDataKeys.PSI_FILE;

/**
 * @author guixiang
 * @des 自动生成代码插件
 * @date 2019/5/9 14:45
 */
public class AutoGenerateCode extends AnAction {
    /**@des logger */
    private static final Logger logger = LoggerFactory.getLogger(AutoGenerateCode.class);
    //master
    @Override
    public void actionPerformed(AnActionEvent e) {
        //current project
        Project project = e.getData(PlatformDataKeys.PROJECT);
        //current class
        PsiFile psiFile = e.getData(CommonDataKeys.PSI_FILE);
        //current classpath
        String classPath = psiFile.getVirtualFile().getPath();
        //current mouse location
        PsiElement psiElement = e.getData(LangDataKeys.PSI_ELEMENT);

        if (psiElement.equals("log")) {
            return ;
        }
        String title = "autogeneratecode";
        String msg = "autogeneratecode will save you time!";

        Messages.showMessageDialog(project, classPath, title, Messages.getInformationIcon());
    }

}
